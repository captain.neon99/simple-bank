//
//  BankWidget.swift
//  BankWidget
//
//  Created by Rizki Abdillah on 13/01/23.
//

import WidgetKit
import SwiftUI
import Intents

struct AppRoute : Decodable {
  let routeName: String,
      routeIcon: String,
      routeURL: String
}
struct Shared:Decodable {
  let isUserLoggedIn: Bool,
      appRoute: [AppRoute]
}

struct Provider: IntentTimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
      SimpleEntry(date: Date(), isUserLoggedIn:false, appRoute:[], configuration: ConfigurationIntent())
    }

    func getSnapshot(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date(), isUserLoggedIn:false, appRoute:[], configuration: configuration)
        completion(entry)
    }

    func getTimeline(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []
      var isUserLoggedIn: Bool = false
      var appRoute:[Any] = []
      let sharedDefaults = UserDefaults.init(suiteName: "group.com.simplebank")
      
                if sharedDefaults != nil {
                      do{
                        let shared = sharedDefaults?.string(forKey: "bankRouteData")
                        if(shared != nil){
                        let data = try JSONDecoder().decode(Shared.self, from: shared!.data(using: .utf8)!)
                          isUserLoggedIn = data.isUserLoggedIn
                          appRoute = data.appRoute
                        }
                      }catch{
                        print(error)
                      }
                }

        // Generate a timeline consisting of five entries an hour apart, starting from the current date.
        let currentDate = Date()
        for hourOffset in 0 ..< 5 {
            let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
            let entry = SimpleEntry(date: entryDate, isUserLoggedIn:isUserLoggedIn, appRoute:appRoute, configuration: configuration)
            entries.append(entry)
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
    let isUserLoggedIn: Bool
    let appRoute: [Any]
    let configuration: ConfigurationIntent
}

struct BankWidgetEntryView : View {
    var entry: Provider.Entry
  
    var size: CGFloat = 45

    var body: some View {
      ZStack{
        Color(red: 0.07, green: 0.08, blue: 0.2)
        
        VStack(alignment: .leading) {
            Text("Simple Bank")
                        .font(.system(size:20))
                        .bold().foregroundColor(.white)
      
          Spacer()
          
          HStack{
            
            Spacer()
            
            VStack{
              Link(destination: URL(string: "bank://home")!, label: {
                Image(systemName:"person")
                  .resizable()
                  .aspectRatio(contentMode: .fit)
                  .padding(/*@START_MENU_TOKEN@*/.all, 12.0/*@END_MENU_TOKEN@*/)
                  .frame(width: size, height: size, alignment: .trailing )
                  .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                  .overlay(
                    Circle().stroke(Color.white , lineWidth: 2)
                  )
                  .foregroundColor(Color.white)
                
              })
              
              Text("Home")
                .font(.system(size:18))
                .foregroundColor(.white)
              
            }
            
            Spacer()
            
            VStack{
              Link(destination: URL(string: "bank://home/card")!, label: {
                Image(systemName:"creditcard")
                  .resizable()
                  .aspectRatio(contentMode: .fit)
                  .padding(/*@START_MENU_TOKEN@*/.all, 12.0/*@END_MENU_TOKEN@*/)
                  .frame(width: size, height: size, alignment: .trailing )
                  .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                  .overlay(
                    Circle().stroke(Color.white , lineWidth: 2)
                  )
                  .foregroundColor(Color.white)
                
              })
              
              Text("+Card")
                .font(.system(size:18))
                .foregroundColor(.white)
              
            }
            
            Spacer()
            
            VStack{
              Link(destination: URL(string: "bank://home/transaction")!, label: {
                Image(systemName:"plus")
                  .resizable()
                  .aspectRatio(contentMode: .fit)
                  .padding(/*@START_MENU_TOKEN@*/.all, 12.0/*@END_MENU_TOKEN@*/)
                  .frame(width: size, height: size, alignment: .trailing )
                  .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                  .overlay(
                    Circle().stroke(Color.white , lineWidth: 2)
                  )
                  .foregroundColor(Color.white)
                
              })
              
              Text("+Trans")
                .font(.system(size:18))
                .foregroundColor(.white)
              
            }
            
            Spacer()
            
            VStack{
              Link(destination: URL(string: "bank://home/about")!, label: {
                Image(systemName:"info")
                  .resizable()
                  .aspectRatio(contentMode: .fit)
                  .padding(/*@START_MENU_TOKEN@*/.all, 12.0/*@END_MENU_TOKEN@*/)
                  .frame(width: size, height: size, alignment: .center )
                  .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                  .overlay(
                    Circle().stroke(Color.white , lineWidth: 2)
                  )
                  .foregroundColor(Color.white)
                
              })
              
              Text("About")
                .font(.system(size:18))
                .foregroundColor(.white)
              
            }
            
            Spacer()
            
          }.frame(
            minWidth: 0,
            maxWidth: .infinity,
            alignment: .center
          )
          
          Spacer()
         
          
        }.padding(.all).frame(
          minWidth: 0,
          maxWidth: .infinity,
          minHeight: 0,
          maxHeight: .infinity,
          alignment: .topLeading
        )
      }
    }
}

struct BankWidget: Widget {
    let kind: String = "BankWidget"

    var body: some WidgetConfiguration {
        IntentConfiguration(kind: kind, intent: ConfigurationIntent.self, provider: Provider()) { entry in
            BankWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("My Widget")
        .description("This is an example widget.")
    }
}

struct BankWidget_Previews: PreviewProvider {
    static var previews: some View {
        BankWidgetEntryView(entry: SimpleEntry(date: Date(), isUserLoggedIn:false, appRoute:[], configuration: ConfigurationIntent()))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
