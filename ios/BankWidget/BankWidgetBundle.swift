//
//  BankWidgetBundle.swift
//  BankWidget
//
//  Created by Rizki Abdillah on 13/01/23.
//

import WidgetKit
import SwiftUI

@main
struct BankWidgetBundle: WidgetBundle {
    var body: some Widget {
        BankWidget()
        BankWidgetLiveActivity()
    }
}
