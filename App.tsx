import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import Store from './src/Redux/Store';
import MainRoutes from './src/Routes/MainRoutes';

export default function App() {
  return (
    <Provider store={Store.stores}>
      <PersistGate persistor={Store.persistor}>
        <MainRoutes />
      </PersistGate>
    </Provider>
  );
}
