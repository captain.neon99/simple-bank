import {
  Dimensions,
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import React from 'react';
import {getTheme} from '../../Utilities/Theme';
import GlobalStyles from '../../Utilities/GlobalStyle';
import CLIcon from '../Common/CLIcon';
import {AccountTypes} from '../../Utilities/Types/AccountTypes';
import {formatAccountingNumber} from '../../Utilities/Helpers/StringTools';

const ColorTheme = getTheme();

type AccountCardPropType = {
  empty?: boolean;
  cardData?: AccountTypes;
  style?: StyleProp<ViewStyle>;
  currentWidth?: Function;
  onPress?: Function;
  width?: number;
};

const AccountCard = (props: AccountCardPropType) => {
  const viewWidth = props.width ?? Dimensions.get('window').width;
  const cardWidth = viewWidth;
  const cardHeight = (cardWidth / 4) * 2.5;

  const disablePress = typeof props.onPress !== 'function';

  return (
    <TouchableOpacity
      onPress={() => props.onPress?.()}
      disabled={disablePress}
      style={[
        props.width ? {width: cardWidth} : {},
        {height: cardHeight},
        styles.baseCardContainer,
        props.empty
          ? styles.cardEmpty
          : {
              backgroundColor: props.cardData?.accountColor
                ? props.cardData?.accountColor
                : ColorTheme.ACCENT,
            },
        props.style,
      ]}>
      {props.empty ? (
        <View style={styles.cardEmptyContent}>
          <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textH3]}>+</Text>
        </View>
      ) : (
        <>
          <View style={styles.flex1}>
            <CLIcon name={props.cardData?.accountLogo ?? 'card-outline'} size={32} />
            <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textH3]}>
              {props.cardData?.accountName ?? 'Account Name'}
            </Text>
          </View>
          <View>
            <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textT1, GlobalStyles.textBold]}>
              {formatAccountingNumber(props.cardData?.accountBalance!)}
            </Text>
            <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textT2, GlobalStyles.textBold]}>
              {props.cardData?.accountID}
            </Text>
          </View>
        </>
      )}
    </TouchableOpacity>
  );
};

export default AccountCard;

const styles = StyleSheet.create({
  baseCardContainer: {
    // backgroundColor: ColorTheme.ACCENT,
    borderRadius: 18,
    padding: 18,
  },
  cardEmpty: {
    borderWidth: 1,
    borderColor: ColorTheme.ACCENT,
  },
  cardEmptyContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardBlue: {
    backgroundColor: ColorTheme.ACCENT,
  },
  flex1: {
    flex: 1,
  },
});
