import {StyleProp, StyleSheet, Text, View, ViewStyle} from 'react-native';
import React from 'react';
import GlobalStyles from '../../Utilities/GlobalStyle';
import CLIcon from '../Common/CLIcon';
import {getTheme} from '../../Utilities/Theme';
import {TransactionTypes} from '../../Utilities/Types/TransactionTypes';
import {useSelector} from 'react-redux';
import {RootStateType} from '../../Redux/Store';
import {formatAccountingNumber, formatDate} from '../../Utilities/Helpers/StringTools';

const ColorTheme = getTheme();

type ActivityCardPropTypes = {
  transaction: TransactionTypes;
  style?: StyleProp<ViewStyle>;
  showOrigin?: boolean;
};

const ActivityCard = (props: ActivityCardPropTypes) => {
  const userData = useSelector((user: RootStateType) => user.user);

  const transaction = props.transaction;
  const isInput = transaction.targetDetail.userID === userData.userID;

  const detail = isInput ? transaction.originDetail : transaction.targetDetail;
  const detailInverse = !isInput ? transaction.originDetail : transaction.targetDetail;

  return (
    <View style={[styles.baseContainer, props.style]}>
      <View
        style={[
          styles.iconContainer,
          {backgroundColor: props.showOrigin ? detailInverse.accountColor : detail.accountColor},
        ]}>
        <CLIcon
          size={34}
          name={props.showOrigin ? detailInverse.accountIcon : detail.accountIcon}
        />
      </View>
      <View style={styles.textGroupSpacing}>
        <View style={styles.textPushDown}>
          <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textT2]}>
            {detail.accountName?.length > 1 ? detail.accountName : 'Missing Account'}
          </Text>
        </View>
        <View style={styles.flex1}>
          <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textT3]}>
            {formatDate(transaction.date)}
          </Text>
        </View>
      </View>
      <View style={styles.textFullSize}>
        <Text
          style={[
            GlobalStyles.TextBaseColor,
            GlobalStyles.textT2,
            isInput ? styles.amountPositive : styles.amountNegative,
          ]}>
          {isInput && '+'}
          {formatAccountingNumber(transaction.amount)}
        </Text>
      </View>
    </View>
  );
};

export default ActivityCard;

const styles = StyleSheet.create({
  baseContainer: {
    flexDirection: 'row',
  },
  iconContainer: {
    padding: 8,
    borderRadius: 12,
  },
  textGroupSpacing: {
    paddingHorizontal: 8,
    flex: 1,
  },
  textPushDown: {flex: 1, justifyContent: 'flex-end'},
  textFullSize: {
    justifyContent: 'center',
  },
  amountPositive: {
    fontWeight: 'bold',
  },
  amountNegative: {
    color: ColorTheme.PRIMARY_DARK,
  },
  flex1: {
    flex: 1,
  },
});
