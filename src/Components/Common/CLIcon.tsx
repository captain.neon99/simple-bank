import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {getTheme} from '../../Utilities/Theme';

const ColorTheme = getTheme();

type CLIconPropTypes = {
  name?: string;
  size?: number;
  color?: string;
};
const CLIcon = (props: CLIconPropTypes) => {
  return (
    <Icon
      name={props.name ?? 'add-outline'}
      size={props.size ?? 24}
      color={props.color ?? ColorTheme.PRIMARY}
    />
  );
};

export default CLIcon;
