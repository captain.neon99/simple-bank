import {
  KeyboardTypeOptions,
  StyleProp,
  StyleSheet,
  Text,
  TextInput,
  View,
  ViewStyle,
} from 'react-native';
import React, {useState} from 'react';
import {getTheme} from '../../Utilities/Theme';

export type EditTextPropTypes = {
  value: string;
  onChangeText: Function;
  label?: string;
  keyboardType?: KeyboardTypeOptions;
  style?: StyleProp<ViewStyle>;
  secured?: boolean;
  isError?: boolean;
};

const ColorTheme = getTheme();

const EditText = (props: EditTextPropTypes) => {
  const [active, setActive] = useState(false);

  const isEmail = props.keyboardType === 'email-address';
  const outlined = active || props.isError;

  return (
    <View style={props.style}>
      {props.label && <Text style={styles.label}>{props.label}</Text>}
      <View
        style={[
          styles.inputContainer,
          outlined && styles.inputOutline,
          active && styles.inputActive,
          props.isError && styles.inputError,
        ]}>
        <TextInput
          style={styles.inputStyle}
          value={props.value}
          onChangeText={t => props.onChangeText(t)}
          onFocus={() => setActive(true)}
          onBlur={() => setActive(false)}
          keyboardType={props.keyboardType ?? 'default'}
          secureTextEntry={props.secured}
          autoCapitalize={isEmail ? 'none' : 'sentences'}
          autoCorrect={!isEmail}
        />
      </View>
    </View>
  );
};

export default EditText;

const styles = StyleSheet.create({
  inputContainer: {
    padding: 12,
    backgroundColor: ColorTheme.SECONDARY_DARK,
    borderWidth: 0,
    borderRadius: 8,
  },
  inputOutline: {
    borderWidth: 1,
    paddingVertical: 11,
  },
  inputActive: {
    borderColor: ColorTheme.SECONDARY,
  },
  inputError: {
    borderColor: ColorTheme.ERROR,
  },
  label: {
    color: ColorTheme.PRIMARY_LIGHT,
    marginBottom: 4,
    // marginLeft: 12,
    fontSize: 16,
  },
  inputStyle: {
    color: ColorTheme.PRIMARY_LIGHT,
  },
});
