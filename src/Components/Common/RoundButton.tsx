import {StyleProp, StyleSheet, Text, TouchableOpacity, View, ViewStyle} from 'react-native';
import React from 'react';
import {getTheme} from '../../Utilities/Theme';
import CLIcon from './CLIcon';

const ColorTheme = getTheme();

type RoundButton = {
  onPress?: Function;
  color?: string;
  active?: boolean;
  style?: StyleProp<ViewStyle>;
  icon?: string;
  size?: number;
};

const RoundButton = (props: RoundButton) => {
  const size = props.size ?? 24;
  return (
    <TouchableOpacity
      style={[
        styles.button,
        props.active && styles.activeButton,
        {backgroundColor: props.color ?? ColorTheme.ACCENT},
        props.icon ? {width: size * 2, height: size * 2} : {width: size, height: size},
        props.style,
      ]}
      onPress={() => props.onPress?.()}>
      {props.icon && <CLIcon name={props.icon} />}
    </TouchableOpacity>
  );
};

export default RoundButton;

const styles = StyleSheet.create({
  button: {
    borderRadius: 1000,
    borderColor: ColorTheme.PRIMARY,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 8,
  },
  activeButton: {
    borderWidth: 2,
  },
});
