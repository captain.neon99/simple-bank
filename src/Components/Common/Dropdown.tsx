import {StyleProp, StyleSheet, Text, TouchableOpacity, View, ViewStyle} from 'react-native';
import React, {useState} from 'react';
import EditText from './EditText';
import CLIcon from './CLIcon';
import GlobalStyles from '../../Utilities/GlobalStyle';
import {getTheme} from '../../Utilities/Theme';

const ColorTheme = getTheme();

export type DropdownDataTypes = {
  label: string;
  value: string;
};

type DropdownPropTypes = {
  label: string;
  dropdownData: DropdownDataTypes[];
  disable?: boolean;
  style?: StyleProp<ViewStyle>;
  onSelected: (item: DropdownDataTypes) => void;
};
const Dropdown = (props: DropdownPropTypes) => {
  const [selected, setSelected] = useState('');
  const [showDropdown, setShowDropdown] = useState(false);
  const [componentWidth, setComponentWidth] = useState(0);
  const [componentHeight, setComponentHeight] = useState(0);

  const itemPressHandler = (selectedItem: DropdownDataTypes) => {
    setShowDropdown(false);
    setSelected(selectedItem.label);
    props.onSelected(selectedItem);
  };

  return (
    <>
      <View
        style={[styles.inputTextContainer, props.style]}
        onLayout={ev => {
          const layout = ev.nativeEvent.layout;
          setComponentWidth(layout.width);
          setComponentHeight(layout.height + layout.y);
        }}>
        <EditText
          value={selected}
          onChangeText={setSelected}
          label={props.label}
          style={styles.inputText}
        />
        {!props.disable && (
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={() => setShowDropdown(!showDropdown)}>
            <CLIcon name={showDropdown ? 'chevron-down-outline' : 'chevron-back-outline'} />
          </TouchableOpacity>
        )}
      </View>
      {showDropdown && (
        <View
          style={[
            {width: componentWidth},
            styles.dropdownContainerStyle,
            {position: 'absolute', top: componentHeight},
          ]}>
          {props.dropdownData.length > 0 ? (
            props.dropdownData.map((item, index) => (
              <TouchableOpacity
                key={index}
                style={styles.dropdownItemStyle}
                onPress={() => itemPressHandler(item)}>
                <Text
                  style={[
                    GlobalStyles.TextBaseColor,
                    GlobalStyles.textT2,
                    item.label === selected && styles.dropdownItemSelectedStyle,
                  ]}>
                  {item.label}
                </Text>
              </TouchableOpacity>
            ))
          ) : (
            <View style={styles.dropdownItemStyle}>
              <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textT2]}>No User Account</Text>
            </View>
          )}
        </View>
      )}
    </>
  );
};

export default Dropdown;

const styles = StyleSheet.create({
  inputTextContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  inputText: {
    flex: 1,
  },
  buttonStyle: {
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'tomato',
    height: 40,
  },
  dropdownContainerStyle: {
    // position: 'absolute',
    backgroundColor: ColorTheme.SECONDARY_LIGHT,
    zIndex: 100,
  },
  dropdownItemStyle: {
    padding: 8,
    backgroundColor: ColorTheme.SECONDARY_LIGHT,
  },
  dropdownItemSelectedStyle: {
    padding: 8,
    backgroundColor: ColorTheme.SECONDARY_DARK,
  },
});
