import {StyleProp, StyleSheet, Text, TouchableOpacity, View, ViewStyle} from 'react-native';
import React from 'react';
import {getTheme} from '../../Utilities/Theme';

type ButtonModeTypes = 'filled' | 'outlined' | 'text';

export type CLButtonPropTypes = {
  onPress: Function;
  label: string;
  style?: StyleProp<ViewStyle>;
  mode?: ButtonModeTypes;
  wrap?: boolean;
};

const ColorTheme = getTheme();

const CLButton = (props: CLButtonPropTypes) => {
  const selectedMode = props.mode ?? 'filled';

  return (
    <TouchableOpacity
      onPress={() => props.onPress()}
      style={[
        styles.buttonContainer,
        selectedMode === 'filled' && styles.buttonModeFilled,
        selectedMode === 'outlined' && styles.buttonModeOutlined,
        props.wrap && styles.labelWrap,
        props.style,
      ]}>
      <Text style={[styles.buttonLabel, selectedMode === 'filled' && styles.labelModeFilled]}>
        {props.label}
      </Text>
    </TouchableOpacity>
  );
};

export default CLButton;

const styles = StyleSheet.create({
  buttonLabel: {
    color: ColorTheme.PRIMARY_DARK,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  labelModeFilled: {
    color: ColorTheme.PRIMARY_LIGHT,
  },
  labelWrap: {
    alignSelf: 'baseline',
  },
  buttonContainer: {
    padding: 12,
    borderRadius: 8,
    // justifyContent: 'center',
  },
  buttonModeFilled: {
    backgroundColor: ColorTheme.ACCENT,
  },
  buttonModeOutlined: {
    borderColor: ColorTheme.ACCENT,
    borderWidth: 1,
  },
});
