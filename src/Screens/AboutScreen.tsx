import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {DashboardStackScreenProps} from '../Routes/MainRoutesTypes';
import GlobalStyles from '../Utilities/GlobalStyle';
import CLIcon from '../Components/Common/CLIcon';
import CLButton from '../Components/Common/Button';

const AboutScreen = ({navigation, route}: DashboardStackScreenProps<'AboutScreen'>) => {
  const backPressHandler = () => {
    navigation.goBack();
  };
  return (
    <View style={[GlobalStyles.SafeAreaBaseContainer, styles.justifyCenter]}>
      <View style={styles.alignCenter}>
        <View>
          <View style={styles.alignCenter}>
            <CLIcon name="bonfire-outline" size={60} />
          </View>
          <Text style={styles.width}>
            <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textH2]}>Simple</Text>
            <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textH2, GlobalStyles.textBold]}>
              {' '}
              Bank
            </Text>
          </Text>
          <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textT3, styles.width]}>
            This apps is part of mini MVP of Casteluke finance game. And as part of test for
            Synergy's admissions
          </Text>
          <View style={styles.alignCenter}>
            <CLButton label="Back" onPress={backPressHandler} />
          </View>
        </View>
      </View>
    </View>
  );
};

export default AboutScreen;

const styles = StyleSheet.create({
  justifyCenter: {justifyContent: 'center'},
  alignCenter: {alignItems: 'center'},
  width: {width: 200, textAlign: 'center', marginBottom: 8},
});
