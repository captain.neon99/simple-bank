import {
  Dimensions,
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {getTheme} from '../Utilities/Theme';
import GlobalStyles from '../Utilities/GlobalStyle';
import AccountCard from '../Components/Cards/AccountCard';
import {useDispatch, useSelector} from 'react-redux';
import {RootStateType} from '../Redux/Store';
import {UserTypes} from '../Utilities/Types/UserTypes';
import {AccountTypes} from '../Utilities/Types/AccountTypes';
import {setUserAccount} from '../Redux/Reducers/UserAccountReducer';
import {DashboardStackScreenProps, RootStackScreenProps} from '../Routes/MainRoutesTypes';
import {useIsFocused} from '@react-navigation/native';
import CLButton from '../Components/Common/Button';
import {logoutUserData} from '../Redux/Reducers/UserReducer';
import {TransactionTypes} from '../Utilities/Types/TransactionTypes';
import ActivityCard from '../Components/Cards/ActivityCard';
import CLIcon from '../Components/Common/CLIcon';

const ColorTheme = getTheme();

const DashboardScreen = ({navigation, route}: DashboardStackScreenProps<'DashboardScreen'>) => {
  const dispatch = useDispatch();
  const focus = useIsFocused();
  const accountData = useSelector((state: RootStateType) => state.backend.userAccountList);
  const userData = useSelector((state: RootStateType) => state.user);
  const userAccountData = useSelector((state: RootStateType) => state.account.userAccount);

  const cardWidth = Dimensions.get('screen').width - 32;

  /**
   * getUserAccount from backend
   * this is backend function :)
   */
  const getUserAccounts = () => {
    const getUserAccount = (userID: string): AccountTypes[] => {
      return accountData.filter(item => item.accountUserID === userID);
    };
    const accountsList = getUserAccount(userData.userID);

    dispatch(setUserAccount(accountsList));
  };

  const getCollectiveTransaction = () => {
    const collectiveTransaction: TransactionTypes[] = [];
    userAccountData.forEach(accounts => {
      accounts.accountTransaction.forEach(transaction => {
        collectiveTransaction.push(transaction);
      });
    });
    return collectiveTransaction;
  };

  const collective = getCollectiveTransaction();

  useEffect(() => {
    if (focus) getUserAccounts();
  }, [focus]);

  const onEmptyCardPressHandler = () => {
    navigation.navigate('CreateAccountScreen');
  };

  const onCardPressHandler = (card: AccountTypes) => {
    navigation.navigate('AccountDetailsScreen', card);
  };

  const logoutPressHandler = () => {
    dispatch(logoutUserData());
  };

  const createPressHandler = () => {
    navigation.navigate('CreateTransactionScreen');
  };

  const infoPressHandler = () => {
    navigation.navigate('AboutScreen');
  };

  return (
    <SafeAreaView style={styles.baseContainer}>
      <View style={styles.baseSpacing}>
        <View style={styles.titleSection}>
          <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textH2, styles.flex1]}>
            Simple Bank
          </Text>
          <TouchableOpacity onPress={infoPressHandler}>
            <CLIcon name="information-circle-outline" />
          </TouchableOpacity>
        </View>
        <View style={styles.generalSpacing}>
          <Text
            style={[
              GlobalStyles.TextBaseColor,
              GlobalStyles.textH3,
              GlobalStyles.textBold,
              styles.generalSpacing,
            ]}>
            Your Card
          </Text>
          <FlatList
            data={userAccountData}
            keyExtractor={(item, index) => index.toString()}
            horizontal
            snapToInterval={cardWidth + 8}
            decelerationRate={'fast'}
            showsHorizontalScrollIndicator={false}
            renderItem={({item}) => (
              <AccountCard
                cardData={item}
                style={styles.cardSpacing}
                onPress={() => onCardPressHandler(item)}
                width={cardWidth}
              />
            )}
            ListFooterComponent={() => (
              <AccountCard
                empty
                style={styles.cardSpacing}
                onPress={onEmptyCardPressHandler}
                width={cardWidth}
              />
            )}
          />
        </View>
        <View style={[styles.generalSpacing, styles.flex1]}>
          <Text
            style={[
              GlobalStyles.TextBaseColor,
              GlobalStyles.textH3,
              GlobalStyles.textBold,
              styles.generalSpacing,
            ]}>
            Recent Activity
          </Text>
          <View style={styles.transactionTitleSection}>
            <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textH3]}>
              Transaction History
            </Text>
            <View style={styles.transactionCreateButtonContainer}>
              <CLButton label="+Transaction" onPress={createPressHandler} mode="text" />
            </View>
          </View>
          <FlatList
            style={styles.flex1}
            data={collective}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <ActivityCard transaction={item} style={styles.transactionItemStyle} showOrigin />
            )}
          />
        </View>
        <CLButton label="Logout" mode="outlined" onPress={logoutPressHandler} />
      </View>
    </SafeAreaView>
  );
};

export default DashboardScreen;

const styles = StyleSheet.create({
  baseContainer: {
    backgroundColor: ColorTheme.BACKGROUND,
    flex: 1,
  },
  baseSpacing: {
    paddingHorizontal: 8,
    flex: 1,
  },
  generalSpacing: {
    marginBottom: 8,
  },
  cardSpacing: {
    marginHorizontal: 4,
  },
  flex1: {flex: 1},
  transactionItemStyle: {marginBottom: 8},
  transactionTitleSection: {flexDirection: 'row', alignItems: 'center'},
  transactionCreateButtonContainer: {flex: 1, alignItems: 'flex-end'},
  titleSection: {flexDirection: 'row', alignItems: 'center'},
});
