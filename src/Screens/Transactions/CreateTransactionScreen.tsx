import {StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import GlobalStyles from '../../Utilities/GlobalStyle';
import {DashboardStackScreenProps} from '../../Routes/MainRoutesTypes';
import CLButton from '../../Components/Common/Button';
import EditText from '../../Components/Common/EditText';
import {CreateTransactionTypes} from '../../Utilities/Types/TransactionTypes';
import {useDispatch, useSelector} from 'react-redux';
import {RootStateType} from '../../Redux/Store';
import Dropdown, {DropdownDataTypes} from '../../Components/Common/Dropdown';
import {transformAccountToDropdownData} from '../../Utilities/Helpers/DataTransformer';
import {getTheme} from '../../Utilities/Theme';
import {makeTransaction} from '../../Redux/Reducers/BackendDataReducer';

const ColorTheme = getTheme();

const CreateTransactionScreen = ({
  navigation,
  route,
}: DashboardStackScreenProps<'CreateTransactionScreen'>) => {
  const dispatch = useDispatch();

  //get account list from backend
  const accountList = useSelector((state: RootStateType) => state.backend.userAccountList);
  const userAccount = useSelector((state: RootStateType) => state.account.userAccount);

  const passedParam = route.params;
  const accountID = passedParam?.accountID;
  const paramEmpty = passedParam?.accountID === undefined;
  const disableSelection = route.params === null;

  const [sourceID, setSourceID] = useState<string | undefined>(accountID);
  const [sourceIDError, setSourceIDError] = useState<string | null>(null);
  const [sourceBalance, setSourceBalance] = useState<number>(passedParam?.balance ?? 0);
  const [targetID, setTargetID] = useState<string>(accountID ?? '');
  const [targetIDError, setTargetIDError] = useState<string | null>(null);
  const [amount, setAmount] = useState<string>('');
  const [amountError, setAmountError] = useState<string | null>(null);
  const [notes, setNotes] = useState<string>();
  const [targetAccountDropdownData, setTargetAccountDropdownData] = useState<DropdownDataTypes[]>(
    [],
  );

  const sourceAccountDropdownData = transformAccountToDropdownData(userAccount);
  const transformTargetData = transformAccountToDropdownData(accountList).filter(
    item => item.value !== sourceID,
  );

  useEffect(() => {
    setTargetAccountDropdownData(transformTargetData);
  }, [sourceID]);

  const backPressHandler = () => {
    navigation.goBack();
  };

  const transferPressHandler = () => {
    const transferAmount = amount ? parseInt(amount) : 0;

    if (sourceID === undefined) {
      setSourceIDError('No account selected');
      return;
    }
    if (targetID === null || targetID.length < 1) {
      setTargetIDError('No account selected');
      return;
    }
    if (targetID === sourceID) {
      setTargetIDError('Cannot transfer the same account');
      return;
    }
    const newTransactionData: CreateTransactionTypes = {
      amount: transferAmount,
      originAccountID: sourceID!,
      targetAccountID: targetID!,
      notes: notes,
    };

    if (transferAmount < 1) {
      setAmountError('Please input positive amount');
      return;
    }

    if (sourceBalance !== null && transferAmount > sourceBalance) {
      setAmountError('Insufficient balance');
      return;
    }

    setAmountError(null);
    setSourceIDError(null);
    setTargetIDError(null);

    dispatch(makeTransaction(newTransactionData));
    navigation.goBack();
  };

  const selectSourceHandler = (selected: DropdownDataTypes) => {
    const transactionBalance = userAccount.find(
      account => account.accountID === selected.value,
    )?.accountBalance;
    setSourceID(selected.value);
    setSourceBalance(transactionBalance ?? 0);
  };

  return (
    <SafeAreaView style={GlobalStyles.SafeAreaBaseContainer}>
      <View style={styles.baseContainer}>
        <CLButton label="Back" onPress={backPressHandler} wrap />
        <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textH3]}>New Transaction</Text>
        <View style={styles.inputSectionContainer}>
          {paramEmpty && (
            <Dropdown
              label="Transfer From Account"
              dropdownData={sourceAccountDropdownData}
              disable={disableSelection}
              style={styles.bottomSpace}
              onSelected={selectSourceHandler}
            />
          )}
          {sourceIDError !== null && (
            <Text style={[GlobalStyles.textT3, styles.errorText]}>{sourceIDError}</Text>
          )}
          <Dropdown
            label="Transfer to"
            dropdownData={targetAccountDropdownData}
            disable={disableSelection}
            style={styles.bottomSpace}
            onSelected={item => setTargetID(item.value)}
          />
          {targetIDError !== null && (
            <Text style={[GlobalStyles.textT3, styles.errorText]}>{targetIDError}</Text>
          )}
          <EditText
            value={amount}
            onChangeText={setAmount}
            label="Amount"
            style={styles.bottomSpace}
            isError={amountError !== null}
          />
          {amountError !== null && (
            <Text style={[GlobalStyles.textT3, styles.errorText]}>{amountError}</Text>
          )}
          <EditText
            value={notes!}
            onChangeText={setNotes}
            label="Notes"
            style={styles.bottomSpace}
          />
        </View>
        <CLButton label="Transfer" onPress={transferPressHandler} />
      </View>
    </SafeAreaView>
  );
};

export default CreateTransactionScreen;

const styles = StyleSheet.create({
  baseContainer: {
    paddingHorizontal: 8,
    flex: 1,
  },
  inputSectionContainer: {
    marginTop: 8,
    flex: 1,
  },
  bottomSpace: {
    marginBottom: 8,
  },
  errorText: {
    color: ColorTheme.PRIMARY,
    backgroundColor: ColorTheme.ERROR,
    padding: 8,
  },
});
