import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {useDispatch} from 'react-redux';
import CLButton from '../Components/Common/Button';
import EditText from '../Components/Common/EditText';
import {logoutUserData} from '../Redux/Reducers/UserReducer';

//Navigation Imports
import {DashboardStackScreenProps} from '../Routes/MainRoutesTypes';
import {getTheme} from '../Utilities/Theme';

const ColorTheme = getTheme();
/**
 * Devs Screen
 */
const DevsPage = ({navigation, route}: DashboardStackScreenProps<'DevsPage'>) => {
  const dispatch = useDispatch();
  const isTest = route.params?.test;

  const handleNavigatePress = () => {
    navigation.navigate('InformationModal');
  };

  const handleLogoutPress = () => {
    dispatch(logoutUserData());
  };
  return (
    <View style={{flex: 1, backgroundColor: ColorTheme.BACKGROUND}}>
      <View style={{flex: 1}}>
        <Text>DevsPage props is {isTest ? isTest : 'undefined'}</Text>
      </View>
      <TouchableOpacity onPress={handleNavigatePress}>
        <View style={{padding: 15, backgroundColor: 'skyblue'}}>
          <Text>NAVIGATE!</Text>
        </View>
      </TouchableOpacity>
      <CLButton onPress={handleLogoutPress} label="logout" />
    </View>
  );
};

export default DevsPage;
