import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

import {RootStackScreenProps} from '../../Routes/MainRoutesTypes';

const ProfileScreen = ({
  navigation,
  route,
}: RootStackScreenProps<'UserProfileScreen'>) => {
  return (
    <View>
      <Text>ProfileScreen</Text>
    </View>
  );
};

export default ProfileScreen;
