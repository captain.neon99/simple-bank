import {FlatList, StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {DashboardStackScreenProps} from '../../Routes/MainRoutesTypes';
import CLButton from '../../Components/Common/Button';
import {SafeAreaView} from 'react-native-safe-area-context';
import {getTheme} from '../../Utilities/Theme';
import GlobalStyles from '../../Utilities/GlobalStyle';
import AccountCard from '../../Components/Cards/AccountCard';
import {useSelector} from 'react-redux';
import {RootStateType} from '../../Redux/Store';
import {AccountTypes} from '../../Utilities/Types/AccountTypes';
import {useIsFocused} from '@react-navigation/native';
import ActivityCard from '../../Components/Cards/ActivityCard';

const ColorTheme = getTheme();

const AccountDetailsScreen = ({
  navigation,
  route,
}: DashboardStackScreenProps<'AccountDetailsScreen'>) => {
  const passedData = route.params;

  const isFocused = useIsFocused();
  const accountData = useSelector((state: RootStateType) => state.backend.userAccountList);

  const [cardData, setCardData] = useState<AccountTypes>();

  const getAccountDetails = () => {
    const getAccount = (accountID: string) => {
      return accountData.find(account => account.accountID === accountID);
    };

    setCardData(getAccount(passedData.accountID)!);
  };

  useEffect(() => {
    if (isFocused) {
      getAccountDetails();
    }
  }, [isFocused]);

  const backPressHandler = () => {
    navigation.goBack();
  };

  const createPressHandler = () => {
    navigation.navigate('CreateTransactionScreen', {
      accountID: passedData.accountID,
      balance: cardData?.accountBalance!,
    });
  };

  return (
    <SafeAreaView style={GlobalStyles.SafeAreaBaseContainer}>
      <View style={styles.baseContainer}>
        <CLButton label="Back" onPress={backPressHandler} wrap />
        <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textH3]}>Account Details</Text>
        <View style={styles.sectionSpacing}>
          <AccountCard cardData={cardData} />
        </View>
        <View style={styles.transactionTitleSection}>
          <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textH3]}>Transaction History</Text>
          <View style={styles.transactionCreateButtonContainer}>
            <CLButton label="new Transaction" onPress={createPressHandler} mode="text" />
          </View>
        </View>
        <FlatList
          style={styles.flex1}
          data={cardData?.accountTransaction}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => (
            <ActivityCard transaction={item} style={styles.transactionItemStyle} />
          )}
        />
      </View>
    </SafeAreaView>
  );
};

export default AccountDetailsScreen;

const styles = StyleSheet.create({
  baseContainer: {paddingHorizontal: 8, flex: 1},
  sectionSpacing: {marginVertical: 8},
  transactionTitleSection: {flexDirection: 'row', alignItems: 'center'},
  transactionCreateButtonContainer: {flex: 1, alignItems: 'flex-end'},
  transactionItemStyle: {marginBottom: 8},
  flex1: {flex: 1},
});
