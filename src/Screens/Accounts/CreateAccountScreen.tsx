import {Dimensions, FlatList, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import {DashboardStackScreenProps} from '../../Routes/MainRoutesTypes';
import CLButton from '../../Components/Common/Button';
import {SafeAreaView} from 'react-native-safe-area-context';
import GlobalStyles, {CardColor} from '../../Utilities/GlobalStyle';
import EditText from '../../Components/Common/EditText';
import AccountCard from '../../Components/Cards/AccountCard';
import {AccountTypes, UserCreateAccountTypes} from '../../Utilities/Types/AccountTypes';
import {useDispatch, useSelector} from 'react-redux';
import {RootStateType} from '../../Redux/Store';
import {getTheme} from '../../Utilities/Theme';
import RoundButton from '../../Components/Common/RoundButton';
import IconList from '../../Utilities/IconList';
import {createUserAccount} from '../../Redux/Reducers/BackendDataReducer';

const ColorTheme = getTheme();

const CreateAccountScreen = ({
  navigation,
  route,
}: DashboardStackScreenProps<'CreateAccountScreen'>) => {
  const dispatch = useDispatch();
  const userData = useSelector((state: RootStateType) => state.user);

  const cardWidth = Dimensions.get('screen').width - 16;

  const [name, setName] = useState<string>();
  const [color, setColor] = useState<string>();
  const [colorSelectedIndex, setColorSelectedIndex] = useState(0);
  const [logo, setLogo] = useState<string>();
  const [logoSelectedIndex, setLogoSelectedIndex] = useState(0);

  const cardData: AccountTypes = {
    accountBalance: 0,
    accountID: '0000 0000 0000',
    accountName: name!,
    accountColor: color,
    accountLogo: logo,
    accountUserID: userData.userID,
    accountTransaction: [],
    accountUserName: userData.userName,
  };

  const backPressHandler = () => {
    navigation.goBack();
  };

  const onColorPressHandler = (selectedColor: string, index: number) => {
    setColorSelectedIndex(index);
    setColor(selectedColor);
  };
  const onIconPressHandler = (selectedIcon: string, index: number) => {
    setLogoSelectedIndex(index);
    setLogo(selectedIcon);
  };

  const createPressHandler = () => {
    if (name?.length === null && name?.length < 1) {
      return;
    }

    const createPayload: UserCreateAccountTypes = {
      accountName: name!,
      accountColor: color,
      accountLogo: logo,
      accountUserID: userData.userID,
    };

    dispatch(createUserAccount(createPayload));
    navigation.goBack();
  };

  return (
    <SafeAreaView style={GlobalStyles.SafeAreaBaseContainer}>
      <View style={styles.baseContainer}>
        <CLButton label="back" onPress={backPressHandler} wrap />
        <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textH3]}>Create New Account</Text>
        <View style={styles.accountCardContainer}>
          <AccountCard cardData={cardData} width={cardWidth} />
        </View>
        <View style={styles.contentContainer}>
          <View style={styles.textGroupContainer}>
            <EditText value={name!} onChangeText={setName} label="Account Name" />
          </View>
          <View style={styles.sectionSpacing}>
            <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textT2, styles.textSpacing]}>
              Card color
            </Text>
            <View style={styles.colorGroup}>
              {CardColor.map((cardColor, index) => (
                <RoundButton
                  key={index}
                  color={cardColor}
                  onPress={() => onColorPressHandler(cardColor, index)}
                  active={colorSelectedIndex === index}
                  style={styles.colorButtonSpacing}
                />
              ))}
            </View>
          </View>

          <View style={styles.sectionSpacing}>
            <Text style={[GlobalStyles.TextBaseColor, GlobalStyles.textT2, styles.textSpacing]}>
              Card Icon
            </Text>
            <FlatList
              data={IconList}
              keyExtractor={(item, index) => index.toString()}
              horizontal
              contentContainerStyle={styles.iconGroupContainer}
              renderItem={({item, index}) => (
                <RoundButton
                  icon={item}
                  style={styles.colorButtonSpacing}
                  onPress={() => onIconPressHandler(item, index)}
                  active={index === logoSelectedIndex}
                />
              )}
            />
          </View>
        </View>
        <CLButton label="Create" onPress={createPressHandler} />
      </View>
    </SafeAreaView>
  );
};

export default CreateAccountScreen;

const styles = StyleSheet.create({
  baseContainer: {
    paddingHorizontal: 8,
    flex: 1,
  },
  contentContainer: {flex: 1},
  accountCardContainer: {
    alignItems: 'center',
  },
  sectionSpacing: {
    marginVertical: 8,
  },
  colorGroup: {
    flexDirection: 'row',
  },
  textGroupContainer: {
    marginTop: 12,
  },
  colorButtonSpacing: {
    marginRight: 4,
  },
  iconGroupContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },
  textSpacing: {
    marginBottom: 12,
  },
});
