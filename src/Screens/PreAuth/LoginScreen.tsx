import {KeyboardAvoidingView, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import {RootStackScreenProps} from '../../Routes/MainRoutesTypes';
import {getTheme} from '../../Utilities/Theme';
import {SafeAreaView} from 'react-native-safe-area-context';
import EditText from '../../Components/Common/EditText';
import CLButton from '../../Components/Common/Button';
import {ValidateEmail} from '../../Utilities/Helpers/StringTools';
import {isAndroid} from '../../Utilities/Helpers/PlatformCheck';
import {useDispatch, useSelector} from 'react-redux';
import {RootStateType} from '../../Redux/Store';
import {UserLoginTypes} from '../../Utilities/Types/BackendTypes';
import {authConfirmLogin} from '../../Redux/Reducers/BackendDataReducer';
import {loginUserData} from '../../Redux/Reducers/UserReducer';
import {UserTypes} from '../../Utilities/Types/UserTypes';

const ColorTheme = getTheme();

const LoginScreen = ({navigation, route}: RootStackScreenProps<'LoginScreen'>) => {
  const dispatch = useDispatch();
  const backendState = useSelector((state: RootStateType) => state.backend);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordSecured, setPasswordSecured] = useState(true);
  const [errorMessage, setErrorMessage] = useState('');
  const [errorID, setErrorID] = useState(0);

  /**
   * This is a login mechanics
   * @param loginDetails
   */
  const verifyLogin = (loginDetails: UserLoginTypes) => {
    //Fetch user types only used inside this function
    type FetchUserTypes = {
      auth: boolean;
      status: number;
      message: string;
      userData?: UserTypes;
    };

    /**
     * this is mainly user verification on backend
     * @returns FetchUserTypes
     */
    const fetchUser = (): FetchUserTypes => {
      const authList = backendState.userAuth;
      const userList = backendState.userList;
      const userAuth = authList.find(user => user.userEmail === loginDetails.userEmail);

      if (userAuth === null) {
        return {auth: false, message: 'email not found', status: 404};
      }
      if (userAuth?.userPassword !== loginDetails.userPassword) {
        return {auth: false, message: 'password not match', status: 403};
      }
      const userData = userList.find(user => user.userEmail === userAuth.userEmail);

      if (userData === null) {
        return {auth: false, message: 'user data missing', status: 404};
      }

      dispatch(authConfirmLogin(loginDetails));
      return {auth: true, userData: userData, message: 'user found and authenticated', status: 200};
    };

    if (!fetchUser().auth) {
      const error = fetchUser();
      setErrorID(error.status === 403 ? 2 : 1);
      setErrorMessage(error.message);
      return;
    }

    dispatch(loginUserData(fetchUser().userData!));
  };

  const loginHandler = () => {
    if (!ValidateEmail(email)) {
      setErrorMessage('Email is invalid');
      setErrorID(1);
      return;
    }

    if (password.length < 6) {
      setErrorMessage('Password is less than 6 characters');
      setErrorID(2);
      return;
    }
    setErrorID(0);
    setErrorMessage('');
    console.log('login data', {email, password});
    verifyLogin({userEmail: email, userPassword: password});
  };

  const registerHandler = () => {
    navigation.navigate('RegisterScreen');
  };

  return (
    <SafeAreaView style={styles.baseContainer}>
      <KeyboardAvoidingView
        behavior={isAndroid ? 'height' : 'padding'}
        style={styles.baseContainer}>
        <View style={styles.headSpace}>
          <Text style={styles.titleText}>Login</Text>
        </View>
        <View style={styles.contentSpace}>
          <EditText
            label="Email"
            value={email}
            onChangeText={setEmail}
            keyboardType={'email-address'}
            isError={errorID === 1}
          />
          <EditText
            label="Password"
            value={password}
            onChangeText={setPassword}
            keyboardType={'default'}
            secured={passwordSecured}
            style={styles.textTopSpacing}
            isError={errorID === 2}
          />
          <View style={styles.buttonSpacing}>
            <CLButton label="Login" onPress={loginHandler} />
            <CLButton
              label="or Register"
              onPress={registerHandler}
              mode="text"
              style={styles.textTopSpacing}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  baseContainer: {
    backgroundColor: ColorTheme.BACKGROUND,
    flex: 1,
  },
  textTopSpacing: {
    marginTop: 12,
  },
  buttonSpacing: {
    marginTop: 28,
  },
  titleText: {
    color: ColorTheme.PRIMARY,
    fontSize: 24,
    fontWeight: 'bold',
  },
  headSpace: {
    flex: 1,
  },
  contentSpace: {flex: 1, padding: 8},
});
