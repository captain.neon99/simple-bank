import {KeyboardAvoidingView, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import {RootStackScreenProps} from '../../Routes/MainRoutesTypes';
import {getTheme} from '../../Utilities/Theme';
import {SafeAreaView} from 'react-native-safe-area-context';
import EditText from '../../Components/Common/EditText';
import CLButton from '../../Components/Common/Button';
import {ValidateEmail} from '../../Utilities/Helpers/StringTools';
import {isAndroid} from '../../Utilities/Helpers/PlatformCheck';
import {useDispatch} from 'react-redux';
import {authRegister} from '../../Redux/Reducers/BackendDataReducer';
import GlobalStyles from '../../Utilities/GlobalStyle';

const ColorTheme = getTheme();

const RegisterScreen = ({navigation, route}: RootStackScreenProps<'RegisterScreen'>) => {
  const dispatch = useDispatch();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordSecured, setPasswordSecured] = useState(true);
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [errorID, setErrorID] = useState(0);

  const registerHandler = () => {
    if (!ValidateEmail(email)) {
      setErrorMessage('Email is invalid');
      setErrorID(1);
      return;
    }

    if (password.length < 6) {
      setErrorMessage('Password is less than 6 characters');
      setErrorID(2);
      return;
    }

    setErrorID(0);
    setErrorMessage('');
    console.log('register data', {email, password, name, phone});

    dispatch(
      authRegister({userEmail: email, userPassword: password, userName: name, userPhone: phone}),
    );
    navigation.goBack();
  };
  const backHandler = () => {
    navigation.goBack();
  };

  return (
    <SafeAreaView style={GlobalStyles.SafeAreaBaseContainer}>
      <KeyboardAvoidingView
        behavior={isAndroid ? 'height' : 'padding'}
        style={styles.baseContainer}>
        <View style={{flex: 1}}>
          <CLButton label="<back" onPress={backHandler} mode="text" wrap style={styles.spacing} />
        </View>
        <View style={styles.baseContainer}>
          <EditText label="User Name" value={name} onChangeText={setName} style={styles.spacing} />
          <EditText
            label="Phone Number"
            value={phone}
            onChangeText={setPhone}
            keyboardType={'phone-pad'}
            style={styles.spacing}
          />
          <EditText
            label="Email"
            value={email}
            onChangeText={setEmail}
            keyboardType={'email-address'}
            style={styles.spacing}
          />
          <EditText
            label="Password"
            value={password}
            onChangeText={setPassword}
            keyboardType={'default'}
            secured={passwordSecured}
            style={styles.spacing}
          />
          <View style={styles.buttonSpacing}>
            <CLButton label="Register" onPress={registerHandler} />
          </View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default RegisterScreen;

const styles = StyleSheet.create({
  baseContainer: {
    flex: 1,
    paddingHorizontal: 8,
  },
  spacing: {
    marginTop: 12,
  },
  buttonSpacing: {
    marginTop: 28,
  },
  flex1: {
    flex: 1,
  },
});
