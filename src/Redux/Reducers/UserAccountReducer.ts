import {
  AccountTypes,
  UserAccountTypes,
  UserCreateAccountTypes,
} from './../../Utilities/Types/AccountTypes';
import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {createUserAccount} from './BackendDataReducer';

export const AppInitialState: UserAccountTypes = {
  userAccount: [],
};

const UserAccountReducer = createSlice({
  name: 'userAccount',
  initialState: AppInitialState,
  reducers: {
    setUserAccount: (state: UserAccountTypes, action: PayloadAction<AccountTypes[]>) => {
      state.userAccount = action.payload;
    },
  },
});

export const {setUserAccount} = UserAccountReducer.actions;
export default UserAccountReducer;
