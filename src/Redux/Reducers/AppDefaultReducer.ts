import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {AppDefaultType} from '../../Utilities/Types/AppDefaultTypes';

export const AppInitialState: AppDefaultType = {
  theme: 'dark',
};

const AppDefaultReducer = createSlice({
  name: 'appDefault',
  initialState: AppInitialState,
  reducers: {},
});

export const {} = AppDefaultReducer.actions;
export default AppDefaultReducer;
