import {combineReducers} from 'redux';
import AppDefaultReducer from './AppDefaultReducer';
import BackendReducer from './BackendDataReducer';
import UserAccountReducer from './UserAccountReducer';
import UserReducer from './UserReducer';

export default combineReducers({
  appDefault: AppDefaultReducer.reducer,
  user: UserReducer.reducer,
  backend: BackendReducer.reducer,
  account: UserAccountReducer.reducer,
});
