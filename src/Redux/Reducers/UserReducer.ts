import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {UserTypes} from '../../Utilities/Types/UserTypes';

export const userInitialState: UserTypes = {
  userAccountNumber: '',
  userEmail: '',
  userID: '',
  userName: '',
  userPhone: '',
};

const UserReducer = createSlice({
  name: 'user',
  initialState: userInitialState,
  reducers: {
    updateUserData: (state: UserTypes, action: PayloadAction<UserTypes>) => {
      console.log('updating with payload: ', action.payload);
      state = {...action.payload, userLastUpdate: Date.now()};
    },
    loginUserData: (state: UserTypes, action: PayloadAction<UserTypes>) => {
      console.log('updating with payload: ', action.payload);
      const data = action.payload;

      return {...state, ...action.payload};
    },
    /**
     * reset user state
     * @returns userInitialState
     */
    clearUserData: () => userInitialState,
    logoutUserData: () => userInitialState,
  },
});

export const {updateUserData, clearUserData, loginUserData, logoutUserData} = UserReducer.actions;
export default UserReducer;
