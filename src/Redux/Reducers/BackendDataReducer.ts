import {TransactionTypes} from './../../Utilities/Types/TransactionTypes';
import {UserRegisterTypes, UserLoginTypes} from '../../Utilities/Types/BackendTypes';
import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import BackendTypes from '../../Utilities/Types/BackendTypes';
import {AccountTypes, UserCreateAccountTypes} from '../../Utilities/Types/AccountTypes';
import {CreateTransactionTypes} from '../../Utilities/Types/TransactionTypes';
import Store from '../Store';
import {UserIdentificationTypes} from '../../Utilities/Types/UserTypes';

export const BackendInitialState: BackendTypes = {
  userList: [
    {
      userAccountNumber: '080989999',
      userEmail: 'telkomnet@instant.com',
      userID: 'user100010001000',
      userName: 'Telkom Net Instant',
      userPhone: '080989999',
    },
  ],
  userAuth: [{userEmail: 'telkomnet@instant.com', userPassword: '111111'}],
  userAccountList: [
    {
      accountBalance: 100000,
      accountName: 'Telkom Account Fund',
      accountID: '123123123123',
      accountTransaction: [],
      accountUserID: 'user100010001000',
      accountUserName: 'Telkom Net Instant',
      accountColor: '#2567F9',
      accountLogo: 'briefcase-outline',
    },
  ],
};

const BackendReducer = createSlice({
  name: 'backend',
  initialState: BackendInitialState,
  reducers: {
    authRegister: (state: BackendTypes, action: PayloadAction<UserRegisterTypes>) => {
      const newID: string = Date.now().toString();
      state.userAuth.push({
        userEmail: action.payload.userEmail,
        userPassword: action.payload.userPassword,
      });
      state.userList.push({
        userAccountNumber: newID,
        userEmail: action.payload.userEmail,
        userID: `user-${newID}`,
        userName: action.payload.userName,
        userPhone: action.payload.userPhone,
        userLastUpdate: Date.now(),
      });
    },
    authConfirmLogin: (state: BackendTypes, action: PayloadAction<UserLoginTypes>) => {
      const userIndex = state.userList.findIndex(
        user => user.userEmail === action.payload.userEmail,
      );

      if (userIndex < 0) return;

      const userData = state.userList[userIndex];
      userData.userLastLogin = Date.now();

      state.userList[userIndex] = userData;
    },
    createUserAccount: (state: BackendTypes, action: PayloadAction<UserCreateAccountTypes>) => {
      const payload = action.payload;
      console.log('new account created', payload);

      const accountUser = state.userList.find(user => user.userID === payload.accountUserID);

      if (accountUser === null) {
        console.log('user not found');

        return {...state};
      }

      const newID = Date.now().toString();
      const newAccountData: AccountTypes = {
        accountBalance: 100000,
        accountName: payload.accountName,
        accountID: newID,
        accountColor: payload.accountColor,
        accountLogo: payload.accountLogo,
        accountTransaction: [
          {
            amount: 100000,
            date: Date.now(),
            targetDetail: {
              accountID: newID,
              accountName: payload.accountName,
              userID: payload.accountUserID,
              accountColor: payload.accountColor,
              accountIcon: payload.accountLogo,
            },
            originDetail: {
              accountID: 'bank-bonus',
              accountName: 'New Card Bonus',
              userID: 'bank',
              accountIcon: 'card-outline',
              accountColor: '#2567F9',
            },
          },
        ],
        accountUserID: payload.accountUserID,
        accountUserName: accountUser?.userName!,
      };
      state.userAccountList.push(newAccountData);
    },
    makeTransaction: (state: BackendTypes, action: PayloadAction<CreateTransactionTypes>) => {
      const payload = action.payload;

      const userAccountList = state.userAccountList;
      const userList = state.userList;

      //find target account and user
      const targetAccountIndex = userAccountList.findIndex(
        account => account.accountID === payload.targetAccountID,
      );

      const targetAccount = userAccountList[targetAccountIndex];
      const targetUser = userList.find(user => user.userID === targetAccount?.accountUserID);

      //find source account and user
      const originAccountIndex = userAccountList.findIndex(
        account => account.accountID === payload.originAccountID,
      );
      const originAccount = userAccountList[originAccountIndex];
      const originUser = userList.find(user => user.userID === originAccount?.accountUserID);

      const originData: UserIdentificationTypes = {
        accountID: payload.originAccountID,
        userID: originUser?.userID!,
        accountName: originAccount?.accountName,
        accountColor: originAccount.accountColor,
        accountIcon: originAccount.accountLogo,
      };
      const targetData: UserIdentificationTypes = {
        accountID: payload.targetAccountID,
        userID: targetUser?.userID!,
        accountName: targetAccount?.accountName,
        accountColor: targetAccount?.accountColor,
        accountIcon: targetAccount?.accountLogo,
      };

      const newTransactionData: TransactionTypes = {
        amount: payload.amount,
        date: Date.now(),
        notes: payload.notes,
        originDetail: originData,
        targetDetail: targetData,
      };

      //origin data mutation
      state.userAccountList[originAccountIndex].accountBalance -= payload.amount;
      state.userAccountList[originAccountIndex].accountTransaction.push(newTransactionData);

      //target data mutation
      const target = state.userAccountList[targetAccountIndex];
      target.accountBalance += payload.amount;
      target.accountTransaction.push(newTransactionData);
    },
  },
});

export const {authConfirmLogin, authRegister, createUserAccount, makeTransaction} =
  BackendReducer.actions;
export default BackendReducer;
