import {CompositeScreenProps, NavigatorScreenParams} from '@react-navigation/native';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {AccountTypes} from '../Utilities/Types/AccountTypes';

type DevsPageParamType = {
  test: boolean;
};

type CreateTransactionScreenParamTypes = {
  accountID: string;
  balance: number;
};

export type DashboardStackParamListTypes = {
  DashboardScreen: undefined;
  DevsPage: DevsPageParamType | undefined;
  CreateAccountScreen: undefined;
  AccountDetailsScreen: AccountTypes;
  CreateTransactionScreen: CreateTransactionScreenParamTypes | undefined;
  AboutScreen: undefined;
};

export type RootStackParamListTypes = {
  DevsPage: DevsPageParamType | undefined; // with props or not
  //PreLogin Screen Group
  LoginScreen: undefined;
  RegisterScreen: undefined;
  //PostLogin Screen Group
  DashboardStack: NavigatorScreenParams<DashboardStackParamListTypes>;
  UserProfileScreen: undefined;
  //Utilities (modal) Screen Group
  InformationModal: undefined;
};

/**
 * Root Stack Screen Props use appropriate screen name
 * @example {navigation, props}: RootStackScreenProps<'Home'>
 */
export type RootStackScreenProps<T extends keyof RootStackParamListTypes> = NativeStackScreenProps<
  RootStackParamListTypes,
  T
>;

/**
 * Dashboard Stack Screen Props use appropriate screen name
 * @example {navigation, props}: DashboardStackScreenProps<'Home'>
 */
export type DashboardStackScreenProps<T extends keyof DashboardStackParamListTypes> =
  CompositeScreenProps<
    NativeStackScreenProps<DashboardStackParamListTypes, T>,
    NativeStackScreenProps<RootStackParamListTypes>
  >;

//idk how to use this actually
declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamListTypes {}
  }
}
