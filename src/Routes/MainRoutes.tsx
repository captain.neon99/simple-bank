import React, {useEffect, useState} from 'react';

import {SafeAreaProvider} from 'react-native-safe-area-context';
import {LinkingOptions, NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {DashboardStackParamListTypes, RootStackParamListTypes} from './MainRoutesTypes';

import DevsPage from '../Screens/DevsPage';
import DashboardScreen from '../Screens/DashboardScreen';
import LoginScreen from '../Screens/PreAuth/LoginScreen';
import RegisterScreen from '../Screens/PreAuth/RegisterScreen';
import ProfileScreen from '../Screens/User/ProfileScreen';
import InformationModal from '../Screens/Modal/InformationModal';
import {useSelector} from 'react-redux';
import {RootStateType} from '../Redux/Store';
import CreateAccountScreen from '../Screens/Accounts/CreateAccountScreen';
import AccountDetailsScreen from '../Screens/Accounts/AccountDetailsScreen';
import CreateTransactionScreen from '../Screens/Transactions/CreateTransactionScreen';
import AboutScreen from '../Screens/AboutScreen';
import SharedGroupPreferences from 'react-native-shared-group-preferences';

const Stack = createNativeStackNavigator<RootStackParamListTypes>();
const StackDashboard = createNativeStackNavigator<DashboardStackParamListTypes>();

export default function MainRoutes() {
  const userData = useSelector((state: RootStateType) => state.user);

  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    if (userData.userID.length > 0) {
      setIsLoggedIn(true);
      saveUserDataToSharedStorage();
    } else {
      setIsLoggedIn(false);
      saveUserDataToSharedStorage();
    }
  }, [userData]);

  const saveUserDataToSharedStorage = async () => {
    try {
      const appGroupIdentifier = 'group.com.simplebank';

      type WidgetRoute = {
        routeName: string;
        routeIcon: string;
        routeURL: string;
      };

      type SharedGroupDataTypes = {
        isUserLoggedIn: boolean;
        appRoute: WidgetRoute[];
      };

      let sharedData: SharedGroupDataTypes = {
        isUserLoggedIn: true,
        appRoute: [
          {
            routeIcon: 'person',
            routeName: 'Home',
            routeURL: 'bank://simplebank.com/home',
          },
          {
            routeIcon: 'creditcard',
            routeName: '+Card',
            routeURL: 'bank://simplebank.com/account',
          },
          {
            routeIcon: 'plus',
            routeName: '+Transfer',
            routeURL: 'bank://simplebank.com/transfer',
          },
          {
            routeIcon: 'info',
            routeName: 'about',
            routeURL: 'bank://simplebank.com/about',
          },
        ],
      };
      await SharedGroupPreferences.setItem('bankRouteData', sharedData, appGroupIdentifier).then(
        res => console.log('res', {res, sharedData}),
      );
    } catch (errorCode) {
      console.log(errorCode);
    }
  };

  const linkingConfig = {
    prefixes: ['bank://'],
    config: {
      screens: {
        UserProfileScreen: 'profile',
        DashboardStack: {
          path: 'home',
          screens: {
            AboutScreen: 'about',
            CreateAccountScreen: 'card',
            CreateTransactionScreen: 'transaction',
          },
        },
      },
    },
  };

  const DashboardNavigationStack = () => {
    return (
      <StackDashboard.Navigator
        initialRouteName="DashboardScreen"
        screenOptions={{headerShown: false}}>
        <StackDashboard.Screen name="DashboardScreen" component={DashboardScreen} />
        <StackDashboard.Screen name="CreateAccountScreen" component={CreateAccountScreen} />
        <StackDashboard.Screen name="AccountDetailsScreen" component={AccountDetailsScreen} />
        <StackDashboard.Screen name="CreateTransactionScreen" component={CreateTransactionScreen} />
        <StackDashboard.Screen name="AboutScreen" component={AboutScreen} />
        <StackDashboard.Screen name="DevsPage" component={DevsPage} />
      </StackDashboard.Navigator>
    );
  };
  /**
   * npx uri-scheme open "bank://simplebank.com/home/about" --ios
   */
  return (
    <SafeAreaProvider>
      <NavigationContainer linking={linkingConfig}>
        <Stack.Navigator screenOptions={{headerShown: false}}>
          {/* Pre/Post Login Screen Isolation */}
          {!isLoggedIn ? (
            <Stack.Group>
              <Stack.Screen name="LoginScreen" component={LoginScreen} />
              <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
            </Stack.Group>
          ) : (
            <Stack.Group screenOptions={{headerShown: false}}>
              <Stack.Screen name="DashboardStack" component={DashboardNavigationStack} />
              <Stack.Screen
                options={{headerShown: true}}
                name="UserProfileScreen"
                component={ProfileScreen}
              />
            </Stack.Group>
          )}
          {/* Utilities Screen Below */}
          <Stack.Group screenOptions={{presentation: 'transparentModal'}}>
            <Stack.Screen name="InformationModal" component={InformationModal} />
          </Stack.Group>
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}
