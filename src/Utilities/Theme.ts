type ThemeColorSchemeTypes = {
  PRIMARY: string;
  PRIMARY_LIGHT: string;
  PRIMARY_DARK: string;
  SECONDARY: string;
  SECONDARY_LIGHT: string;
  SECONDARY_DARK: string;
  TERTIARY: string;
  ACCENT: string;
  SUCCESS: string;
  ERROR: string;
  WARNING: string;
  INFO: string;
  BACKGROUND: string;
};

// type DefaultThemeColorTypes = 'light' | 'dark';

type DefaultColorTypes = {
  light: ThemeColorSchemeTypes;
  dark: ThemeColorSchemeTypes;
};

/**
 * Default color lists
 *
 * to add/remove new color palette please just add or remove object
 * object key will be named as Theme Color Types
 */
export const DefaultColor: DefaultColorTypes = {
  light: {
    //Currently using Dark ONLY
    PRIMARY: '#e3f2fd',
    PRIMARY_LIGHT: '#ffffff',
    PRIMARY_DARK: '#D9D9D9',
    SECONDARY: '#2567F9',
    SECONDARY_LIGHT: '#858BE9',
    SECONDARY_DARK: '#23265A',
    TERTIARY: '#8225F9',
    ACCENT: '#2567F9',
    SUCCESS: '#15B825',
    ERROR: '#FF2D2D',
    WARNING: '#FFB017',
    INFO: '#2567F9',
    BACKGROUND: '#121433',
  },
  dark: {
    PRIMARY: '#e3f2fd',
    PRIMARY_LIGHT: '#ffffff',
    PRIMARY_DARK: '#D9D9D9',
    SECONDARY: '#2567F9',
    SECONDARY_LIGHT: '#858BE9',
    SECONDARY_DARK: '#23265A',
    TERTIARY: '#8225F9',
    ACCENT: '#2567F9',
    SUCCESS: '#15B825',
    ERROR: '#FF2D2D',
    WARNING: '#FFB017',
    INFO: '#2567F9',
    BACKGROUND: '#121433',
  },
};

export type DefaultThemeColorTypes = keyof typeof DefaultColor;

/**
 * Get Theme color Palette
 * @default 'dark' theme
 * @param theme
 * @returns color palette (DefaultColorTypes)
 */
export const getTheme = (theme?: DefaultThemeColorTypes) => {
  return DefaultColor[theme ?? 'dark'];
};
