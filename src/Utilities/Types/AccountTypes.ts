import {TransactionTypes} from './TransactionTypes';
export type AccountTypes = {
  accountID: string;
  accountName: string;
  accountLogo?: string;
  accountColor?: string;
  accountBalance: number;
  accountTransaction: TransactionTypes[];
  accountUserID: string;
  accountUserName: string;
};
export type UserAccountTypes = {
  userAccount: AccountTypes[];
};
export type UserCreateAccountTypes = {
  accountName: string;
  accountLogo?: string;
  accountColor?: string;
  accountUserID: string;
};
