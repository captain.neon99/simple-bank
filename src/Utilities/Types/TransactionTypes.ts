import {UserIdentificationTypes} from './UserTypes';

export type TransactionTypes = {
  originDetail: UserIdentificationTypes;
  targetDetail: UserIdentificationTypes;
  amount: number;
  notes?: string;
  date: Date | number;
};

export type CreateTransactionTypes = {
  targetAccountID: string;
  originAccountID: string;
  amount: number;
  notes?: string;
};
