import {DefaultThemeColorTypes} from '../Theme';

export type AppDefaultType = {
  theme: DefaultThemeColorTypes;
};
