export type UserTypes = {
  userName: string;
  userID: string;
  userEmail: string;
  userPhone: string;
  userAccountNumber: string;
  userLastLogin?: number;
  userLastUpdate?: number;
};

export type UserIdentificationTypes = {
  userID: string;
  accountName: string;
  accountID: string;
  accountIcon?: string;
  accountColor?: string;
};
