import {AccountTypes, UserAccountTypes} from './AccountTypes';
import {UserTypes} from './UserTypes';

type UserAuthTypes = {
  userEmail: string;
  userPassword: string;
};

export type UserRegisterTypes = {
  userName: string;
  userEmail: string;
  userPhone: string;
  userPassword: string;
};

export type UserLoginTypes = {
  userEmail: string;
  userPassword: string;
};

type BackendTypes = {
  userList: UserTypes[];
  userAuth: UserAuthTypes[];
  userAccountList: AccountTypes[];
};

export default BackendTypes;
