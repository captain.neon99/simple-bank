import {StyleSheet} from 'react-native';
import {getTheme} from './Theme';

const ColorTheme = getTheme();

const GlobalStyles = StyleSheet.create({
  TextBaseColor: {
    color: ColorTheme.PRIMARY,
  },
  textBold: {
    fontWeight: 'bold',
  },
  textH1: {
    fontSize: 32,
  },
  textH2: {
    fontSize: 28,
  },
  textH3: {
    fontSize: 24,
  },
  textT1: {
    fontSize: 22,
  },
  textT2: {
    fontSize: 16,
  },
  textT3: {
    fontSize: 14,
  },
  SafeAreaBaseContainer: {
    backgroundColor: ColorTheme.BACKGROUND,
    flex: 1,
  },
});

export const CardColor = [
  ColorTheme.ACCENT,
  ColorTheme.TERTIARY,
  ColorTheme.WARNING,
  ColorTheme.SUCCESS,
];

export default GlobalStyles;
