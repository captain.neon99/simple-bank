const IconList = [
  'card-outline',
  'airplane-outline',
  'boat-outline',
  'bonfire-outline',
  'bus-outline',
  'briefcase-outline',
];
export default IconList;
