import {DropdownDataTypes} from './../../Components/Common/Dropdown';
import {AccountTypes} from './../Types/AccountTypes';
export const transformAccountToDropdownData = (accountList: AccountTypes[]) => {
  const dropdownData: DropdownDataTypes[] = [];

  if (accountList.length < 1) {
    return [];
  }

  accountList.forEach(account => {
    const newData: DropdownDataTypes = {
      label: account.accountName,
      value: account.accountID,
    };
    dropdownData.push(newData);
  });

  return dropdownData;
};
