import moment from 'moment';

export const ValidateEmail = (email: string) => {
  return email.match(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  );
};

/**
 * Validate and modify email to standard
 * @param email
 * @returns email with lowercase, trim, and google.com if there is no google.com yet
 */
export const SanitizeEmail = (email: string) => {
  if (!ValidateEmail(email)) {
    email = `${email}@google.com`;
  }
  return email.replace(' ', '').toLowerCase().trim();
};

type FormatTypes = {
  currency: string;
  local?: string;
};

/**
 * Format number into currency
 * @param number number (amount of money)
 */
export const formatAccountingNumber = (
  number: number,
  format: FormatTypes = {currency: 'IDR', local: 'ID'},
) => {
  const formatter = new Intl.NumberFormat(format.local, {
    style: 'currency',
    currency: format.currency,
  });
  return formatter.format(number);
};

export const formatDate = (date: Date | number, format?: string) =>
  moment(date).format(format ?? 'DD MMMM YYYY');
