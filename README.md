# Simple Bank Apps

iOS Widget Project

## Project Introduction

This project is part of a test for Synergy.com.sg admission and as a challenge and exercise for myself as a developer to develop working POC (Proof of Concept) of any project requirement.
As a start of a project, I intend to create something out of this project that may be useful for myself. This project is a full-fledged POC from UI functionality and its mechanics.

## Simple Bank Apps introduction

This mobile app is a standalone app (no connectivity required to run). All user management, authentication, and all data is saved into the app's local storage. This is to reduce the development time and simplify the workflow. All of the UI was built from scratch albeit the minimum tweaks to its spacing in some areas due to time limitation. All types and routes are easy to find since most of the types (except components specific) are placed into its own types file.

## Project Goals

- [x] [Project requirement](https://synergyfa.notion.site/Take-Home-Assignment-iOS-App-Widgets-9882e4b56b2848b989177224d011e32c) is a guideline to what to be achieved into this project or MVP for this project at current stage.
- [x] Create an Apps and its functionalities such as Login, Register, Dashboard etc. mimic minimum bank mobile apps functionalities to enhance User Experience.
- [x] Create the Widget for iOS
- [x] Enable React Native deep-links capabilities through React Navigation
- [x] Setup each of icon in Widget to go to specific page in the apps\*
- [x] Successfully move the page through widget

notes:

- User need to authenticate to go to other pages
- This is due to currently I am cannot successfully send the data from React Native to iOS Widget due to account problem
- User had to be login / register to open the full apps
- Email has to be valid email format ex. [test.test@domain.com]

one default account

```
email: telkomnet@instant.com
password: 111111
```

## How to Install

- [ ] [Set up React Native environment](hhttps://reactnative.dev/docs/environment-setup) to the machine
- [ ] Open Terminal and go to the project Root Directory
- [ ] Setup the Project package and dependencies by running the command below

```
npm run prepare-project
```

## How to run the apps

Start the React Native bundler (Metro) and run iOS command

```
npm run ios
```

this command should run Metro Bundler while building the apps

---

# Project Discovery

## Challenges and Issues

1. The ultimate challenge for this is actually in the Widget implementation, Since this is my first time creating a Widget and still green into how the widgets UI work and its mechanics or full-extend of its capabilities.
2. Sending data from React Native to iOS require AppGroup in the native side which need Developer account to set up.
3. Due to recent changes of version of React Native and xCode, there is discrepancies to what some resource have and what I currently have in terms of information on documentation.
